﻿---

## About

This is a sample code on how to implement Serial Communication using TF6340.

Beckhoff hardware: EL6001, EL6002, EL6021, EL6022, KL6001, KL6002, KL6021, KL6022.
Other hardware: USB to RS232/RS485 converter.

---

## Pre-requisite

Download and install TF6340 TC3 Serial Comm Function from Beckhoff website.

Download serial terminal software for testing.
For example: [Hercules](https://www.hw-group.com/software/hercules-setup-utility)

---

## How to use




---

## Help

Contact support@beckhoff.com.sg